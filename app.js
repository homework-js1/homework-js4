function getFirstUserNumber() {
    let userFirstNumber = +prompt("Enter first number:");
    while (userFirstNumber === 0 || Number(isNaN(userFirstNumber))) {
        userFirstNumber = +prompt("Repeat enter first number:", userFirstNumber);
    }
    return userFirstNumber;
}
function getSecondUserNumber() {
    let userSecondNumber = +prompt("Enter second number:");
    while (userSecondNumber === 0 || Number(isNaN(userSecondNumber))) {
        userSecondNumber = +prompt("Repeat enter second number:", userSecondNumber);
    }
    return userSecondNumber;
}
function getMathSymbol () {
    let userMathSymbol = prompt("Enter math symbol such as +,-,*,/:");
    while (!/^[\-+*(/)]/.test(userMathSymbol)) {
        userMathSymbol = prompt("Repeat enter math symbol such as +,-,*,/:", userMathSymbol);
    }
    return userMathSymbol;
}
function calcNumbers() {
    const mathSymbol = getMathSymbol ();
    switch (mathSymbol) {
        case '+': {
            return  getFirstUserNumber() + getSecondUserNumber();
        }
        case '-': {
            return  getFirstUserNumber() - getSecondUserNumber();
        }
        case '*': {
            return  getFirstUserNumber() * getSecondUserNumber();
        }
        case '/': {
            return  getFirstUserNumber() / getSecondUserNumber();
        }
    }
}

console.log(calcNumbers());